<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-rtl.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-theme-rtl.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/terme.css">
	<link rel="stylesheet" href="assets/css/rtl.css">

</head>
<body>
	<?php include 'templates/header/1.php'; ?>
	<main class="main">
		<section class="top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-8">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						    <div class="carousel-inner" role="listbox">
						        <div class="item active">
						            <img src="assets/img/slider_01.jpg" height="385" width="750" alt="...">
						        </div>
						        <div class="item">
						            <img src="assets/img/slider_02.jpg" alt="...">
						        </div>
						    </div><!-- carousel-inner -->

						    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						       <i class="fa fa-angle-left"></i>
						    </a>
						    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						       <i class="fa fa-angle-right"></i>
						    </a>
						</div><!-- carousel slide -->
					</div><!-- col-xs-8 -->
					<div class="col-xs-4">
							<div class="selection">
								<h4>اخبار برگزیده</h4>
								<a href="#" class="more pull-right">بیشتر</a>
								<ul>
									<li>
										<div class="thumb"><img src="assets/img/top_new_01.jpg" height="75" width="100" alt=""></div>
										<h3><a href="">تماشا کنید: ایده مایکروسافت برای تماشای بازی‌ها در آینده</a></h3>
										<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
									</li>
									<li>
										<div class="thumb"><img src="assets/img/top_new_02.jpg" height="75" width="100" alt=""></div>
										<h3><a href="">تصویر کیس‌های جدید گلکسی اس 7 و ال جی جی 5 فاش شد</a></h3>
										<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
									</li>
									<li>
										<div class="thumb"><img src="assets/img/top_new_03.jpg" height="75" width="100" alt=""></div>
										<h3><a href="">مدیریت فایل ها در اپلیکیشن یونیورسال Your Files در ویندوزفون</a></h3>
										<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
									</li>
								</ul>
							</div><!-- selection -->
						</div><!-- col-xs-4 -->
				</div>
			</div><!-- container -->
		</section><!-- top -->
		<section class="latest_news">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12">
						<div class="body">
							<div class="title">جدیدترین اخبار</div>
							<ul id="typed-strings">
								<li><a href="">تماشا کنید: ایده مایکروسافت برای تماشای بازی‌ها در آینده</a></li>
								<li><a href="">تصویر کیس‌های جدید گلکسی اس 7 و ال جی جی 5 فاش شد</a></li>
								<li><a href="">مدیریت فایل ها در اپلیکیشن یونیورسال Your Files در ویندوزفون</a></li>
							</ul>
							<span id="typed" style="white-space:pre;"></span>
						</div><!-- body -->
					</div><!-- col-xs-12 -->
				</div><!-- row -->
			</div><!-- container-fluid -->
		</section><!-- latest_news -->

		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-8">
					<div class="home_content">
						<div class="box style_1">
							<div class="title">
								<a href="#" class="more pull-right">مطالب بیشتر</a>
								<h4>ورزشی</h4>
								<h6>مطالب بیشتر در بخش ورزشی</h6>
							</div><!-- title -->
							<div class="row">
								<div class="col-xs-6">
									<div class="big_post">
										<div class="thumb"><img src="assets/img/post_01.jpg" height="250" width="420" alt=""></div>
										<h2><a href="#">تماشا کنید: ایده مایکروسافت برای تماشای بازی‌ها در آینده</a></h2>
										<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
										<div class="excerpt">مهاجم سابق منچسترسیتی معتقد است که با آمدن پپ گواردیولا به این تیم، سیتیزن ها می توانند سطح خود را به عنوان یک باشگاه فوتبال بالاتر ببرند.</div>
									</div><!-- big_post -->
								</div><!-- col-xs-6 -->
								<div class="col-xs-6">
									<div class="small_post">
										<ul>
											<li>
												<div class="thumb"><img src="assets/img/top_new_01.jpg" height="75" width="100" alt=""></div>
												<h2><a href="#">تصویر کیس‌های جدید گلکسی اس 7 و ال جی جی 5 فاش شد</a></h2>
												<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
											</li>
											<li>
												<div class="thumb"><img src="assets/img/top_new_02.jpg" height="75" width="100" alt=""></div>
												<h2><a href="#">مدیریت فایل ها در اپلیکیشن یونیورسال Your Files در ویندوزفون</a></h2>
												<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
											</li>
											<li>
												<div class="thumb"><img src="assets/img/top_new_03.jpg" height="75" width="100" alt=""></div>
												<h2><a href="#">The world is changed! i feel in the water, i feel in the earth</a></h2>
												<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
											</li>
											<li>
												<div class="thumb"><img src="assets/img/top_new_04.jpg" height="75" width="100" alt=""></div>
												<h2><a href="#">گواردیولا می تواند مسی را به سیتی بیاورد</a></h2>
												<div class="time"><i class="fa fa-clock-o"></i> جمعه 25 بیمهن 1394</div>
											</li>
										</ul>
									</div>
								</div><!-- col-xs-6 -->
							</div><!-- row -->
						</div><!-- box -->
					</div><!-- home_content -->
				</div><!--col-xs-8-->
				<div class="col-xs-4"></div><!--col-xs-4-->
			</div><!-- row -->
		</div><!-- container -->
	</main>

	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/typed.js"></script>
	<script type="text/javascript" src="assets/js/terme.js"></script>
</body>
</html>