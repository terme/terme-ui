<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/css/terme.css">

</head>
<body>
	<?php include 'templates/header/1.php'; ?>
	<main class="main">
		<section class="top">
			<div class="container">
				<div class="row">
					<div class="col-xs-8">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						    <div class="carousel-inner" role="listbox">
						        <div class="item active">
						            <img src="assets/img/slider_01.jpg" height="385" width="750" alt="...">
						        </div>
						        <div class="item">
						            <img src="assets/img/slider_02.jpg" alt="...">
						        </div>
						    </div><!-- carousel-inner -->

						    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						       <i class="fa fa-angle-left"></i>
						    </a>
						    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						       <i class="fa fa-angle-right"></i>
						    </a>
						</div><!-- carousel slide -->
					</div><!-- col-xs-8 -->
					<div class="col-xs-4">
						<div class="selection">
							<h4>Top News</h4>
							<a href="#" class="more pull-right">More</a>
							<ul>
								<li>
									<div class="thumb"><img src="assets/img/top_new_01.jpg" height="75" width="100" alt=""></div>
									<h3><a href="">Google Ranked As The World's Most Valuable Brands</a></h3>
									<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
								</li>
								<li>
									<div class="thumb"><img src="assets/img/top_new_02.jpg" height="75" width="100" alt=""></div>
									<h3><a href="">2016 Mac Pro release date rumours & specs</a></h3>
									<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
								</li>
								<li>
									<div class="thumb"><img src="assets/img/top_new_03.jpg" height="75" width="100" alt=""></div>
									<h3><a href="">The world is changed! i feel in the water, i feel in the earth</a></h3>
									<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
								</li>
							</ul>
						</div><!-- selection -->
					</div><!-- col-xs-4 -->
				</div>
			</div><!-- container -->
		</section><!-- top Section -->
		<section class="latest_news">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="body">
							<div class="title">Latest News</div>
							<ul id="typed-strings">
								<li><a href="">The world is changed! i feel in the water, i feel in the earth</a></li>
								<li><a href="">2016 Mac Pro release date rumours & specs</a></li>
								<li><a href="">Google Ranked As The World's Most Valuable Brands</a></li>
							</ul>
							<span id="typed" style="white-space:pre;"></span>
						</div><!-- body -->
					</div><!-- col-xs-12 -->
				</div><!-- row -->
			</div><!-- container -->
		</section><!-- latest_news -->

		<div class="container">
			<div class="row">
				<div class="col-xs-8">
					<div class="home_content">
						<div class="box style_1">
							<div class="title">
								<a href="#" class="more pull-right">More</a>
								<h4>Sport</h4>
								<h6>Defeated Panthers to get back</h6>
							</div><!-- title -->
							<div class="body">
								<div class="row">
									<div class="col-xs-6">
										<div class="big_post">
											<div class="thumb"><img src="assets/img/post_01.jpg" height="250" width="420" alt=""></div>
											<h2><a href="#">The world is changed! i feel in the water, i feel in the earth</a></h2>
											<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
											<div class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat ut laoreet dolore magna aliquam erat volutpat ut laoreet dolore magna  aliquam erat volutpat.</div>
										</div><!-- big_post -->
									</div><!-- col-xs-6 -->
									<div class="col-xs-6">
										<div class="small_post">
											<ul>
												<li>
													<div class="thumb"><img src="assets/img/top_new_01.jpg" height="75" width="100" alt=""></div>
													<h2><a href="#">The world is changed! i feel in the water, i feel in the earth</a></h2>
													<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
												</li>
												<li>
													<div class="thumb"><img src="assets/img/top_new_02.jpg" height="75" width="100" alt=""></div>
													<h2><a href="#">The world is changed! i feel in the water, i feel in the earth</a></h2>
													<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
												</li>
												<li>
													<div class="thumb"><img src="assets/img/top_new_03.jpg" height="75" width="100" alt=""></div>
													<h2><a href="#">The world is changed! i feel in the water, i feel in the earth</a></h2>
													<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
												</li>
												<li>
													<div class="thumb"><img src="assets/img/top_new_04.jpg" height="75" width="100" alt=""></div>
													<h2><a href="#">Why You Should Avoid Sass @extend</a></h2>
													<div class="time"><i class="fa fa-clock-o"></i> Friday, 27 April 2016</div>
												</li>
											</ul>
										</div>
									</div><!-- col-xs-6 -->
								</div><!-- row -->
							</div><!-- body -->
						</div><!-- box -->

						<div class="box style_2">
							<div class="title">
								<a href="#" class="more pull-right">More</a>
								<h4>Shop</h4>
								<h6>Defeated Panthers to get back</h6>
							</div><!-- title -->
							<div class="body">
								<ul class="shop-carousel">
									<li>
										<div class="product">
											<div class="thumb">
												<img src="assets/img/product_01.jpg" height="180" width="190" alt="">
												<a href="#" class="add_to_cart">Add to cart</a>
												<a href="#" class="permalink">Description</a>
											</div>
											<div class="info">
												<h3><a href="#">Product name</a></h3>
												<div class="price">2,500$</div>
											</div><!-- info -->
										</div><!-- product -->
									</li>
									<li>
										<div class="product">
											<div class="thumb">
												<img src="assets/img/product_02.jpg" height="180" width="190" alt="">
												<a href="#" class="add_to_cart">Add to cart</a>
												<a href="#" class="permalink">Description</a>
											</div>
											<div class="info">
												<h3><a href="#">Product name</a></h3>
												<div class="price">2,500$</div>
											</div><!-- info -->
										</div><!-- product -->
									</li>
									<li>
										<div class="product">
											<div class="thumb">
												<img src="assets/img/product_03.jpg" height="180" width="190" alt="">
												<a href="#" class="add_to_cart">Add to cart</a>
												<a href="#" class="permalink">Description</a>
											</div>
											<div class="info">
												<h3><a href="#">Product name</a></h3>
												<div class="price">2,500$</div>
											</div><!-- info -->
										</div><!-- product -->
									</li>
									
								</div><!-- shop-carousel -->
							</div><!-- body -->
						</div><!-- box -->

					</div><!-- home_content -->
				</div><!--col-xs-8-->
				<div class="col-xs-4"></div><!--col-xs-4-->
			</div><!-- row -->
		</div><!-- container -->
	</main>
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/typed.js"></script>
	<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="assets/js/terme.js"></script>
</body>
</html>